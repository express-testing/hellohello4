/**
 * Flash - Custom Scripts For This Site
 * ---------------------------------------
 */
flash.ready(function(){
	new cookiesManager({
        cookies_groups: [
            {
                name: 'Analytics',
                description: '<p>The cookies we use are “analytical” cookies. They allow us to recognise and count the number of visitors and to see how visitors move around the site when they are using it. This helps us to improve the way our website works, for example by ensuring that users are finding what they are looking for easily.</p>',
                cookies: ['_ga']
            },
            {
                name: 'Marketing',
                description: '<p>We [also] use cookies for commercial and advertising messages tailored to your interests based on your browsing habits</p>',
                cookies: ['hubspot']
            }
        ]
    }); 
    
    // Fix the position of the body when lightboxes are open
    document.body.addEventListener('lightboxVisible', function(){
        var scroll_top = window.pageYOffset;
        document.body.style.position = 'fixed';
        document.body.style.top = scroll_top * -1 + 'px';
    });
    
    document.body.addEventListener('lightboxClosed', function(){
        var scroll_top = parseInt(document.body.style.top) * -1;
        document.body.style.position = 'relative';
        document.body.style.top = 0;
        console.log(scroll_top)
        window.scrollBy(0, scroll_top);
    });
});