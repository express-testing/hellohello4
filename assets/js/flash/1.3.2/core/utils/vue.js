/**
 * Loading Vue.js and executing a callback
 */

flashCore.prototype.vue = function(callback) {
    flash.loadScript('https://cdn.jsdelivr.net/npm/vue', callback);
}