flashCore.prototype.slideDown = function(el, timing, callback) {
    var self = this;

    if(!timing) {
        timing = 500;
    }
    var animation_timing_ms = timing;

    var el_max_height = 0;
    var transitions = el.style['transition'];

    el.style.display               = 'block';
    el_max_height                  = self.getHeight(el) + 'px';
    el.style['transition']         = 'max-height ' + animation_timing_ms + 'ms ease-in-out';
    el.style.overflowY             = 'hidden';
    el.style.maxHeight             = '0';

    setTimeout(function() {
        el.style.maxHeight = el_max_height;
    }, 10);

    setTimeout(function(){
        el.style.transitions = transitions;
        
        if(callback) {
            callback();
        }
    }, timing);
}