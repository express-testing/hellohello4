flashCore.prototype.fadeIn = function(el, timing, callback) {
    var self = this;

    if(!timing) {
        timing = 500;
    }
    var animation_timing_ms = timing;
    var transitions = el.style['transition'];

    if(!el.style.opacity) {
        el.style.opacity = 0;
    }
    el.style['transition']          = 'opacity ' + animation_timing_ms + 'ms ease-in-out';
    if(el.style.display === 'none' || !el.style.display) {
        el.style.display                = 'block';
    }
    setTimeout(function(){
        el.style.opacity                = 1;  
    }, 1);

    if(callback) {
        setTimeout(function(){
            el.style.transitions = transitions;
            callback();
        }, timing);
    }    
}